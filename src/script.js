import angular from './img/angular.svg';
import aurelia from './img/aurelia.svg';
import backbone from './img/backbone.svg';
import ember from './img/ember.svg';
import jsBadger from './img/js-badge.svg';
import react from './img/react.svg';
import vue from './img/vue.svg';
import newTux from './img/NewTux.svg';
export function init() {

  /**
   * Access the <ul> with class of .deck
   * @type {Element}
   */
  const deck = document.querySelector('.deck');

  /**
   * Access the modal
   * @type {HTMLElement}
   */
  const modal = document.getElementById('modal');
  /**
   * Acess the reset button
   * @type {Element}
   */
  const reset = document.querySelector('.reset-btn');
  /**
   * Access the playAgainButton
   * @type {Element}
   */
  const playAgain = document.querySelector('.play-again-btn');

  /**
   * Select the class moves-conter and chage it's HTML
   * @type {Element}
   */
  const movesCount = document.querySelector('.moves-counter');
  /**
   * Access star rating
   * @type {NodeListOf<Element>}
   */

  const star = document.getElementById('star-rating').
      querySelectorAll('.star');
  let starCount = 3;

  let game;

  let moves = 0;

  const starCounter = document.querySelector('.timer');

  let time;

  let minutes = 0;

  let seconds = 0;

  let timeStart = false;

  let actionFlip = new Event("FLIP");
  let actionUnFlip = new Event("UNFLIP");
  let actionMatch = new Event("MATCH");

//funzione che mischia gli oggetti nel array
  function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;

  }

// startGame apendo gli li le immagini
  function startGame(deck) {
    //invoke shuffle function and store in variable
    this.matched = [];
    this.opened = [];


     this.deckCards = [
      angular,
      angular,
      aurelia,
      aurelia,
      backbone,
       backbone,
       ember,
       ember,
       jsBadger,
       jsBadger,
       react,
       react,
       vue,
       vue,
       newTux,
       newTux];

    const shuffleDeck = shuffle(this.deckCards);
    // Iterate over deck of cards array
    for (let i = 0; i < shuffleDeck.length; i++) {
      //create the <li> tags
      let liTag = document.createElement('LI');
      //Give <li> class of card

      liTag.classList.add('card');
      liTag.setAttribute("id", shuffleDeck[i]);
      //Create the <img> tags
      const addImage = document.createElement('img');
      // Append <img> to <li>
      liTag.appendChild(addImage);
      // set the img src path with the shuffled deck
      addImage.setAttribute('src',  shuffleDeck[i]);
      // add an alt tag the image
      addImage.setAttribute('alt', 'image of coding');
      //Update the new <li> to the deck <ul>

      //function addEvent() {
      /**
       * Azione che scoperchia la carta
       */

      liTag.addEventListener('FLIP', function(event) {

        liTag.classList.add('flip');
        event.stopPropagation();

      }, false);

      /**
       * Azione che richiude la carta
       */
      liTag.addEventListener('UNFLIP', function(event) {

        liTag.classList.remove('flip');
        event.stopPropagation();

      }, false);
      /**
       * Azione che mette la carta in match
       */
      liTag.addEventListener('MATCH', function(event) {

        liTag.classList.add('match');
        event.stopPropagation();

      }, false);

      deck.appendChild(liTag);
    }

  }

// toglie tutte le carte dle carte al campo di gioco
  function removeCard() {
    while (deck.hasChildNodes()) {

      deck.removeChild(deck.firstChild);

    }

  }

  function resetEverything() {
    stopTime();
    timeStart = false;
    seconds = 0;
    minutes = 0;
    starCounter.innerHTML = '<i class=\'fa fa-hourglass-start\' > </i>' +
        'Timer: 00:00';
    //Reset start count and the add the class back to shoe again
    star[1].firstElementChild.classList.add('fast-star');
    star[2].firstElementChild.classList.add('fast-star');

    starCount = 3;

    moves = 0;

    movesCount.innerHTML = 0;

    // Clear the deck

    removeCard();
    //Create a new deck
    game = new startGame(deck);
  }

  game = new startGame(deck);

  function timer() {
    time = setInterval(function() {
      seconds++;
      if (seconds === 60) {
        minutes++;
        seconds = 0;
      }
      starCounter.innerHTML = '<i class = \'fa fa-hourglass-start\'></i>'
          + 'Timer: ' + minutes + ' : ' + seconds + ' ';

    }, 1000);
  }

  function stopTime() {
    clearInterval(time);
  }

  function movesCounter() {
    movesCount.innerHTML++;
    moves++;
  }

  function startRating() {
    if (moves === 14) {
      star[2].firstElementChild.classList.remove('fa-star');

      starCount--;

    }
    if (moves === 18) {
      star[1].firstElementChild.classList.remove('fa-star');
      starCount--;
    }
  }

  function addStats() {
    const stats = document.querySelector('.modal-conter');
    for (let i = 0; i <= 3; i++) {
      const statsElement = document.createElement('p');
      statsElement.classList.add('stats');
      stats.appendChild(statsElement);
    }
    let p = stats.querySelectorAll('p.stats');
    p[0].innerHTML = 'time to complete: ' + minutes + ' Minutes and ' +
        seconds + ' Seconds';
    p[1].innerHTML = 'Moves Taken: ' + moves;
    p[2].innerHTML = 'Your Star Rating is: ' + starCount + ' out of 3 ';
  }

  function displayModal() {
    const modalClose = document.getElementsByClassName('close')[0];

    modal.style.display = 'block';

    window.onclick = function(event) {
      if (event.target === modal) {

        modal.style.display = 'none';
      }
    };
  }

  function winGame() {
    if (game.matched.length === 16) {
      stopTime();
      addStats();
      displayModal();
    }
  }

  deck.addEventListener("click", function(evt) {
    if (evt.target.nodeName === 'LI') {
      console.log(evt.target.nodeName + 'Was clicked');
      if (timeStart === false) {
        timeStart = true;
        timer();
      }
      //flipCard();
      console.log(evt.target);
      evt.target.dispatchEvent(actionFlip);
      // match();
      console.log(game);
      if (game.opened.length === 0 || game.opened.length === 1) {
        game.opened.push(evt.target.firstElementChild);
      }
      if (game.opened.length === 2) {
        document.body.style.pointerEvents = 'none';
      }
      if (game.opened.length === 2 &&
          game.opened[0].src === game.opened[1].src) {

        // addToOpened();
        setTimeout(function() {
          game.opened[0].parentElement.dispatchEvent(actionMatch);
          game.opened[1].parentElement.dispatchEvent(actionMatch);

          game.matched.push(...game.opened);
          console.log(document.body.style.pointerEvents);
          document.body.style.pointerEvents = 'auto';
          winGame();
          game.opened = [];
        }, 600);
        movesCounter();

        startRating();
      } else if (game.opened.length === 2 &&
          game.opened[0].src !== game.opened[1].src) {
        //noMatch();
        setTimeout(function() {
          game.opened[0].parentElement.dispatchEvent(actionUnFlip);
          game.opened[1].parentElement.dispatchEvent(actionUnFlip);
          console.log(document.body.style.pointerEvents);
          document.body.style.pointerEvents = 'auto';
          game.opened = [];
        }, 700);
        movesCounter();
        startRating();
      }

    }
  });

  reset.addEventListener('click', resetEverything);

  playAgain.addEventListener('click', function() {
    modal.style.display = 'none';
    resetEverything();
  });
}



var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
module.exports = {
  mode: 'development',
 // entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'index_bundle.js'
  },
  module: {
    rules: [
      {test: /\.css$/,  use: ['style-loader', 'css-loader'],},
      {test: /\.(png|jpe?g|gif|svg)$/i, use: "url-loader"},
    ],
  },
  plugins: [
      new HtmlWebpackPlugin({
        template: './src/game.html',
      })


  ],
};


